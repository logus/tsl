(function () {
  'use strict';
  angular
    .module('tsl')
    .run(run);

  run.$inject = ['$ionicPlatform', 'bluetoothService'];

  function run($ionicPlatform, bluetoothService) {

    $ionicPlatform.ready(platformReady);

    function platformReady() {
      if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
        cordova.plugins.Keyboard.disableScroll(true);
      }
      if (window.StatusBar) {
        StatusBar.styleDefault();
      }
      bluetoothService.init();
    }
  }


})();
