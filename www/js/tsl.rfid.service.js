(function () {
  'use strict';

  angular.module('tsl')
    .factory('rfidService', rfidService);

  rfidService.$inject = ['$q', 'CODE_INIT', 'CODE_BLUETOOTH_DEVICE_LIST', 'CODE_READER_CONNECT', 'CODE_READER_DISCONNECT'];

  function rfidService($q, CODE_INIT, CODE_BLUETOOTH_DEVICE_LIST, CODE_READER_CONNECT, CODE_READER_DISCONNECT) {

    var config = {
        "defaultMode": 3,
        "defaultScan": 2,
        "ReaderGeigerPower": 0,
        "ReaderMultiplePower": 0,
        "ReaderSinglePower": 0,
        "ReaderSoundEnabled": true,
        "ReaderVibrateEnabled": true,
        "qGeigerScan": 1,
        "qMultiScan": 5,
        "qSingleScan": 1,
        "powerMode": "db",
        "multiplePowerDB": 10,
        "singlePowerDB": 10,
        "selectGeigerScan": "No",
        "selectMultiScan": "No",
        "selectSingleScan": "Yes",
        "sessionGeigerScan": "S0",
        "sessionMultiScan": "S1",
        "sessionSingleScan": "S0",
        "targetGeigerScan": "B",
        "targetMultiScan": "A",
        "targetSingleScan": "A"
      },
      service = {
        init: init,
        list: list,
        connect: connect,
        disconnect: disconnect
      };

    return service;

    function init() {

      var deferred = $q.defer();

      if (window.cordova) {
        RFIDInterface.executeCommand(CODE_INIT, '', successInit, errorInit);
      }

      function successInit(result) {
        deferred.notify(JSON.parse(result));
      }

      function errorInit(error) {
        deferred.reject(JSON.parse(error));
      }

      return deferred.promise;
    }

    function list() {
      var deferred = $q.defer();
      if (window.cordova) {
        RFIDInterface.executeCommand(CODE_BLUETOOTH_DEVICE_LIST, '', successList, errorList);
      } else {
        errorList();
      }

      function successList(result) {
        var response = JSON.parse(result);
        if (response.hasOwnProperty('lstDevices') && angular.isArray(response.lstDevices) && response.lstDevices.length) {
          deferred.resolve(response.lstDevices);
        } else {
          errorList();
        }
      }

      function errorList() {
        deferred.reject();
      }

      return deferred.promise;
    }

    function connect(device) {

      var deferred = $q.defer();

      if (window.cordova) {
        RFIDInterface.executeCommand(CODE_READER_CONNECT, {
          name: device,
          configuration: config
        }, successConnect, errorConnect);
      }
      else {
        errorConnect();
      }

      function successConnect(result) {
        deferred.notify(JSON.parse(result));
      }

      function errorConnect(error) {
        deferred.reject(JSON.parse(error));
      }

      return deferred.promise;

    }

    function disconnect() {
      var deferred = $q.defer();

      if (window.cordova) {
        RFIDInterface.executeCommand(CODE_READER_DISCONNECT, null, successDisconnect, errorDisconnect);
      }
      else {
        errorConnect();
      }

      function successDisconnect(result) {
        deferred.resolve(JSON.parse(result));
      }

      function errorDisconnect(error) {
        deferred.reject(JSON.parse(error));
      }

      return deferred.promise;
    }

  }
})();
