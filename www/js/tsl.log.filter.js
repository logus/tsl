(function () {
  'use strict';
  angular
    .module('tsl')
    .filter('log', log);

  function log() {
    return filter;
    function filter(input) {
      return JSON.toString(input);
    }
  }

})();
