(function () {
  'use strict';
  angular
    .module('tsl', ['ionic', 'jsonFormatter', 'bluetooth']);
})();
