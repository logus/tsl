(function () {
  'use strict';

  angular
    .module('tsl')
    .controller('HomeController', HomeController);

  HomeController.$inject = ['$scope', '$timeout', '$ionicPlatform', '$ionicModal', '$ionicPopup', 'bluetoothService'];

  function HomeController($scope, $timeout, $ionicPlatform, $ionicModal, $ionicPopup, bluetoothService) {

    $scope.log = [];
    $scope.devices = [];
    $scope.connected = false;
    $scope.configured = false;
    $scope.bluetooth = false;
    $scope.tags = [];
    $scope.sound = true;
    $scope.config = {
      "defaultMode": "3",
      "defaultScan": "2",
      "ReaderGeigerPower": 10,
      "ReaderMultiplePower": 10,
      "ReaderSinglePower": 10,
      "ReaderSoundEnabled": true,
      "ReaderVibrateEnabled": true,
      "qGeigerScan": "1",
      "qMultiScan": "5",
      "qSingleScan": "1",
      "powerMode": "db",
      "multiplePowerDB": 10,
      "singlePowerDB": 10,
      "selectGeigerScan": "No",
      "selectMultiScan": "No",
      "selectSingleScan": "Yes",
      "sessionGeigerScan": "S0",
      "sessionMultiScan": "S1",
      "sessionSingleScan": "S0",
      "targetGeigerScan": "B",
      "targetMultiScan": "A",
      "targetSingleScan": "A"
    };

    $scope.commands = {
      102: {code: 102, name: 'List', params: null},
      200: {code: 200, name: 'Connect', params: {name: null, configuration: $scope.config}},
      201: {code: 201, name: 'Disconnect', params: null},
      202: {code: 202, name: 'Is Connected', params: null},
      203: {code: 203, name: 'Set Configuration', params: $scope.config},
      204: {code: 204, name: 'Get Configuration', params: null},
      209: {code: 209, name: 'Pull Trigger', params: {active: true, timer: 500}},
      210: {
        code: 210,
        name: 'Set Sound',
        params: {value: $scope.config.ReaderSoundEnabled, params: [$scope.config.ReaderSoundEnabled]}
      },
      212: {
        code: 212,
        name: 'Set Vibration',
        params: {value: $scope.config.ReaderVibrateEnabled, params: [$scope.config.ReaderVibrateEnabled]}
      }
    };

    $scope.selected = {command: $scope.commands[102], device: null};

    $scope.$watch('selected.device', function () {
      if ($scope.selected.device) {
        $scope.commands[200].params.name = $scope.selected.device.serialNumber;
      }
    });

    $scope.$watch('config.ReaderSoundEnabled', function () {
      $scope.commands[210].params.value = $scope.config.ReaderVibrateEnabled;
      $scope.commands[210].params.params[0] = $scope.config.ReaderVibrateEnabled;
    });

    $scope.$watch('config.ReaderSoundEnabled', function () {
      $scope.commands[212].params.value = $scope.config.ReaderSoundEnabled;
      $scope.commands[212].params.params[0] = $scope.config.ReaderSoundEnabled;
    });

    $scope.$watchCollection('config', function () {
      $scope.commands[200].params.configuration = $scope.config;
      $scope.commands[203].params = $scope.config;
    });

    $scope.runCommand = runCommand;
    $scope.clearLog = clearLog;
    $scope.clearTags = clearTags;

    $scope.$on('$ionicView.afterEnter', afterEnter);

    window.addEventListener('BluetoothStatus.enabled', bluetoothEnabled);
    window.addEventListener('BluetoothStatus.disabled', bluetoothDisabled);

    function clearLog() {
      $scope.log = [];
    }

    function clearTags() {
      $scope.tagsModal.hide().then(function () {
        $scope.tags = [];
      });
    }

    function afterEnter() {

      $ionicPlatform.ready(platformReady);

      function platformReady() {

        $scope.bluetooth = bluetoothService.isEnabled();
        if (!$scope.bluetooth) {
          bluetoothDisabled();
        }
      }

      initModals();

    }

    function initModals() {
      $ionicModal.fromTemplateUrl('templates/config.html', {
        scope: $scope
      }).then(function (modal) {
        $scope.configModal = modal;
      });
      $ionicModal.fromTemplateUrl('templates/tags.html', {
        scope: $scope
      }).then(function (modal) {
        $scope.tagsModal = modal;
      });
    }

    function alertBluetooth() {
      $scope.bluetoothAlert = $ionicPopup.alert({
        title: 'Bluetooth disabled',
        template: 'Please turn on your bluetooth to sync with the reader.'
      });
    }

    function bluetoothEnabled() {
      $scope.bluetooth = true;
      if ($scope.bluetoothAlert && $scope.bluetoothAlert.hasOwnProperty('close')) {
        $scope.bluetoothAlert.close();
      }
    }

    function bluetoothDisabled() {
      $scope.bluetooth = false;
      $scope.connected = null;
      $scope.configured = null;
      alertBluetooth();
    }

    function runCommand() {
      console.log($scope.config);
      if (window.cordova && RFIDInterface) {
        RFIDInterface.executeCommand($scope.selected.command.code, $scope.commands[$scope.selected.command.code].params, successCommand, errorCommand);
      } else {
        $ionicPopup.alert({
          title: 'Not in device',
          template: 'Please use a real device for testing.'
        });
      }
    }

    function successCommand(result) {
      $timeout(function () {
        var response = JSON.parse(result);
        $scope.log.push(response);
        if (response.hasOwnProperty('lstDevices') && angular.isArray(response.lstDevices) && response.lstDevices.length) {
          $scope.devices = response.lstDevices;
        }
      });

    }

    function errorCommand(result) {
      $timeout(function () {
        var response = JSON.parse(result);
        $scope.log.push(response);
      });
    }

  }
})();
