(function () {
  'use strict';

  angular
    .module('tsl')
    .config(config);

  config.$inject = ['$stateProvider', '$urlRouterProvider'];

  function config($stateProvider, $urlRouterProvider) {

    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'templates/home.html',
        controller: 'HomeController'
      });

    $urlRouterProvider.otherwise('/');

  }

})();
