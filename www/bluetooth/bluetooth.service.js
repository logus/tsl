(function () {
  'use strict';

  angular
    .module('bluetooth')
    .factory('bluetoothService', bluetoothService);

  function bluetoothService() {

    var service = {
      init: init,
      isEnabled: isEnabled
    };

    return service;

    function init() {
      if (window.cordova && cordova.plugins.BluetoothStatus) {
        cordova.plugins.BluetoothStatus.initPlugin();
      }
    }

    function isEnabled() {
      if (window.cordova && cordova.plugins.BluetoothStatus) {
        return cordova.plugins.BluetoothStatus.BTenabled;
      } else {
        return false;
      }
    }

  }

})();
