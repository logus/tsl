# Ionic TSL Tester
 
### Requirements
 
* NodeJS
* Ionic
* Gulp
* Bower
 
### Installation
 
Install node dependencies 
 
```bash
npm install
```

Install bower dependencies

```bash
bower install
```

Compile Styles

```bash
gulp sass
```

Prepare iOS platform project

```bash
ionic cordova prepare ios
```

Now go to XCode and change the provision profile.

Build Application

```bash
ionic cordova build ios
```

Run Application

```bash
ionic cordova run ios
```